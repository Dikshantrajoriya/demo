import 'loggingInterceptor.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'conection_validator.dart';


class ApiProvider {
  Dio _dio = new Dio();

  ApiProvider() {
    BaseOptions options = BaseOptions(
        baseUrl: "WebApiConstaint.BASE_URL",
        receiveTimeout: 5000,
        connectTimeout: 5000);

    _dio = Dio(options);
    _dio.interceptors.add(LoggingInterceptor());
  }

  get flutterToast => null;
  Future<Response?> requestGetForApi(BuildContext context, String url) async {
    if(await new ConnectionValidator().check()){
      //header("Access-Control-Allow-Origin: *");
      try {
        Response response = await _dio.get(url,
            options: Options(contentType: Headers.jsonContentType, headers: {
              "Access-Control-Allow-Origin": "*", // Required for CORS support to work
              "Access-Control-Allow-Credentials": true, // Required for cookies, authorization headers with HTTPS
              "Access-Control-Allow-Headers": "Origin,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
              "Access-Control-Allow-Methods": "POST, OPTIONS"
            })
        );

        print(response);
        return response;

      }catch (error) {
        print(error.toString());
        return null;
      }
    }else{
      flutterToast.showToast(msg: 'Please check network connection and try again !');
      return null;
    }


  }


  Future<Response?> requestPostForApi(BuildContext context, Map<dynamic, dynamic>  dictParameter , String url , String authKey,String authentication) async {

    if(await new ConnectionValidator().check()){

      try {
        Response response = await _dio.post(url , data:dictParameter,

            options: Options(contentType: Headers.jsonContentType, headers: {
              "accesstoken": "uyh678l-07b8-4386-8f6b-c0c11c1e3khb",
              "authkey" : authKey,
              "authentication" : authentication
            })
        );

        print(response);

        return response;
      } catch (error) {
        // _errorHandler(error,context);
        return null;
      }
    }else{
      flutterToast.showToast(msg: 'Please check network connection and try again !');
      return null;
    }



  }
}