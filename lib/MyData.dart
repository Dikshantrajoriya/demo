class MyData {
  int error = 0;
  String message = "";
  List<Category> category = [];

  MyData({required this.error, required this.message, required this.category});

  MyData.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    if (json['category'] != null) {
      category = <Category>[];
      json['category'].forEach((v) {
        category.add(new Category.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['category'] = this.category.map((v) => v.toJson()).toList();
    return data;
  }
}

class Category {
  int id = 0;
  String name = "";
  String image = "";

  Category({required this.id, required this.name, required this.image});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['image'] = this.image;
    return data;
  }
}