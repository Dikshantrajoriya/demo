import 'package:dio/dio.dart';


class LoggingInterceptor extends Interceptor{

  int _maxCharactersPerLine = 200;
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    // TODO: implement onRequest
    // super.onRequest(options, handler);
    print("--> ${options.method} ${options.path}");
    print("Content type: ${options.contentType}");
    print("<-- END HTTP");
    // handler.resolve(Response(requestOptions: options));
    return super.onRequest(options,handler);

  }

}