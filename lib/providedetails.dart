import 'package:demo/MyData.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'api_provider.dart';

class ProvideDetails extends StatefulWidget {
  @override
  _ProvideDetails createState() => _ProvideDetails();
}

class _ProvideDetails extends State<ProvideDetails> {

  List<Category> myDataList = [];

@override
  void initState() {
    super.initState();
    getProductList(context);
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Places"),
        ),
        body: ListView(
          children: <Widget>[
            Column(
              children: [
                Container(
                  height: 662,
                  child: GridView.count(
                    crossAxisCount: 2,
                    children: List.generate(myDataList.length, (index) {
                      return Container(
                        width: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 6.0, top: 6.0),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.blue,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 6,
                                )
                              ],
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Image.network(myDataList[index].image),
                                Container(
                                  height: 61,
                                  width: double.infinity,
                                  color: Colors.black,
                                  child: Center(
                                    child: Text(
                                      myDataList[index].name,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 28),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void getProductList(BuildContext context) {

    ApiProvider apiProvider = ApiProvider();
    apiProvider.requestGetForApi(context, "https://saltindia.org/api.php?action=type").then((response)  {
      print(response);
      if(response != null){
        {
          print(response.data);
          var myResponse = MyData.fromJson(response.data);

          if(myResponse != null && myResponse.category.length > 0){
            myDataList = myResponse.category;
          }
        }
      }

    });
  }
}
